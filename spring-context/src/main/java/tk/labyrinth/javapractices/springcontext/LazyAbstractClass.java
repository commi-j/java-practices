package tk.labyrinth.javapractices.springcontext;

import org.springframework.context.annotation.Lazy;

@Lazy
public abstract class LazyAbstractClass {
	// empty
}
