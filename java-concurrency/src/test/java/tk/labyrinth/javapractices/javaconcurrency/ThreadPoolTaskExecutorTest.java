package tk.labyrinth.javapractices.javaconcurrency;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class ThreadPoolTaskExecutorTest {

	@Test
	@Timeout(1)
	void testLimitedQueueAndMaxThreads() {
		AtomicInteger maxOngoing = new AtomicInteger(0);
		AtomicInteger rejected = new AtomicInteger(0);
		//
		ThreadPoolTaskExecutor executor = getConfiguredThreadPoolTaskExecutor(maxOngoing, rejected, 5, 4);
		//
		IntStream.range(0, 15).forEach(index -> executor.execute(() -> sleep(500)));
		//
		sleep(100);
		//
		// 15 started: 4 in progress, 5 queued, 6 rejected
		Assertions.assertEquals(4, maxOngoing.get());
		Assertions.assertEquals(6, rejected.get());
	}

	@Test
	@Timeout(1)
	void testLimitedQueueAndUnlimitedMaxThreads() {
		AtomicInteger maxOngoing = new AtomicInteger(0);
		AtomicInteger rejected = new AtomicInteger(0);
		//
		ThreadPoolTaskExecutor executor = getConfiguredThreadPoolTaskExecutor(maxOngoing, rejected, 5, null);
		//
		IntStream.range(0, 15).forEach(index -> executor.execute(() -> sleep(500)));
		//
		sleep(100);
		//
		// 15 started: 10 in progress, 5 queued
		Assertions.assertEquals(10, maxOngoing.get());
		Assertions.assertEquals(0, rejected.get());
	}

	@Test
	@Timeout(1)
	void testUnlimitedQueueAndMaxThreads() {
		AtomicInteger maxOngoing = new AtomicInteger(0);
		AtomicInteger rejected = new AtomicInteger(0);
		//
		ThreadPoolTaskExecutor executor = getConfiguredThreadPoolTaskExecutor(maxOngoing, rejected, null, null);
		//
		IntStream.range(0, 15).forEach(index -> executor.execute(() -> sleep(500)));
		//
		sleep(100);
		//
		// 15 started: 2 in progress, 13 queued
		Assertions.assertEquals(2, maxOngoing.get());
		Assertions.assertEquals(0, rejected.get());
	}

	private static ThreadPoolTaskExecutor getConfiguredThreadPoolTaskExecutor(
			AtomicInteger maxOngoing,
			AtomicInteger rejected,
			@Nullable Integer queueCapacity,
			@Nullable Integer maxPoolSize) {
		AtomicInteger ongoing = new AtomicInteger(0);
		//
		ThreadPoolTaskExecutor result = new ThreadPoolTaskExecutor();
		//
		result.setCorePoolSize(2);
		if (maxPoolSize != null) {
			result.setMaxPoolSize(maxPoolSize);
		}
		if (queueCapacity != null) {
			result.setQueueCapacity(queueCapacity);
		}
		result.setRejectedExecutionHandler((r, executor) -> rejected.incrementAndGet());
		result.setTaskDecorator(runnable -> () -> {
			int ongoingValue = ongoing.incrementAndGet();
			maxOngoing.updateAndGet(current -> Math.max(current, ongoingValue));
			try {
				runnable.run();
			} finally {
				ongoing.decrementAndGet();
			}
		});
		//
		result.initialize();
		//
		return result;
	}

	// FIXME: Move to ThreadUtils.
	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}
}
