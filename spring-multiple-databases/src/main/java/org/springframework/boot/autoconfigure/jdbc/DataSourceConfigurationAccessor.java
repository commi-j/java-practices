package org.springframework.boot.autoconfigure.jdbc;

import javax.sql.DataSource;

public class DataSourceConfigurationAccessor extends DataSourceConfiguration {

	public static <T> T createDataSource(DataSourceProperties properties, Class<? extends DataSource> type) {
		return DataSourceConfiguration.createDataSource(properties, type);
	}
}
