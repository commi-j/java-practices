package tk.labyrinth.javapractices.springkafka;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Slf4j
@Testcontainers
public class SimpleKafkaTest {

	@Container
	private final KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse(
			"confluentinc/cp-kafka:latest"));

	@Test
	void test() throws ExecutionException, InterruptedException {
		int messageNumber = 10;
		//
		SpringApplication springApplication = new SpringApplication(
				KafkaAutoConfiguration.class,
				SimpleKafkaListener.class,
				SimpleKafkaProducer.class,
				TestCompletionHandle.class);
		springApplication.setDefaultProperties(Map.of(
				"spring.kafka.bootstrapServers", kafkaContainer.getBootstrapServers(),
				"spring.kafka.consumer.autoOffsetReset", "earliest",
				"testRun.messageNumber", messageNumber));
		//
		Instant startedAt = Instant.now();
		//
		ConfigurableApplicationContext applicationContext = springApplication.run();
		TestCompletionHandle testCompletionHandle = applicationContext.getBean(TestCompletionHandle.class);
		//
		testCompletionHandle.getFuture().get();
		//
		logger.info("Test finished in {}ms", Duration.between(startedAt, Instant.now()).toMillis());
	}
}
