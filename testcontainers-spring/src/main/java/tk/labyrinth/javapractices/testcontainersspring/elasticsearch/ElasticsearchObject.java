package tk.labyrinth.javapractices.testcontainersspring.elasticsearch;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Builder(toBuilder = true)
@Document(indexName = "elasticsearch_object")
@Value
public class ElasticsearchObject {

	String code;

	String name;

	// TODO: Wanna it to be UUID, ES gives conversion error. Looks like we need smarter converter.
	@Id
	String uid;
}
