package tk.labyrinth.javapractices.springkafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;

import java.util.concurrent.ForkJoinPool;

@RequiredArgsConstructor
@Slf4j
public class SimpleKafkaProducer implements ApplicationRunner {

	private final KafkaTemplate<Void, String> kafkaTemplate;

	@Value("${testRun.messageNumber}")
	private Integer messageNumber;

	@Override
	public void run(ApplicationArguments args) {
		ForkJoinPool.commonPool().execute(() -> {
			for (int i = 0; i < messageNumber; i++) {
				logger.info("Sending #{}", i);
				kafkaTemplate.send(MessageBuilder
						.withPayload("hello")
						.setHeader(KafkaHeaders.TOPIC, "topic.test")
						.build());
			}
			logger.info("Sending finished");
		});
	}
}
