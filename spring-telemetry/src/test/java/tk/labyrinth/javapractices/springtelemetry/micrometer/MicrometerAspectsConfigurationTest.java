package tk.labyrinth.javapractices.springtelemetry.micrometer;

import io.micrometer.core.annotation.Counted;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Objects;

@SpringBootTest
class MicrometerAspectsConfigurationTest {

	@Test
	void testMetricsWithAspectsConfiguration() {
		SimpleMeterRegistry simpleMeterRegistry = new SimpleMeterRegistry();
		Metrics.addRegistry(simpleMeterRegistry);
		//
		new SpringApplication(
				AopAutoConfiguration.class,
				MicrometerAspectsConfiguration.class,
				TestApplicationRunner.class)
				.run();
		//
		Assertions.assertEquals(
				1,
				simpleMeterRegistry.getMeters().stream()
						.filter(meter -> Objects.equals(meter.getId().getName(), "annotated_run"))
						.count());
		Assertions.assertEquals(
				1,
				simpleMeterRegistry.getMeters().stream()
						.filter(meter -> Objects.equals(meter.getId().getName(), "direct_run"))
						.count());
	}

	@Test
	void testMetricsWithoutAspectsConfiguration() {
		SimpleMeterRegistry simpleMeterRegistry = new SimpleMeterRegistry();
		Metrics.addRegistry(simpleMeterRegistry);
		//
		new SpringApplication(
				AopAutoConfiguration.class,
				TestApplicationRunner.class)
				.run();
		//
		Assertions.assertEquals(
				0,
				simpleMeterRegistry.getMeters().stream()
						.filter(meter -> Objects.equals(meter.getId().getName(), "annotated_run"))
						.count());
		Assertions.assertEquals(
				1,
				simpleMeterRegistry.getMeters().stream()
						.filter(meter -> Objects.equals(meter.getId().getName(), "direct_run"))
						.count());
	}

	static class TestApplicationRunner implements ApplicationRunner {

		@Timed
		@Counted(value = "annotated_run")
		@Override
		public void run(ApplicationArguments args) {
			Counter fooCounter = Metrics.counter("direct_run");
			fooCounter.increment();
		}
	}
}