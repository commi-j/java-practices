package tk.labyrinth.javapractices.testcontainersspring.kafka;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;
import tk.labyrinth.javapractices.testcontainersspring.EventRegistry;

import java.time.Instant;
import java.util.List;

/**
 * https://www.baeldung.com/spring-boot-testcontainers-integration-test
 * https://www.baeldung.com/spring-boot-kafka-testing
 * https://www.baeldung.com/spring-boot-redis-testcontainers
 */
@ContextConfiguration(initializers = {KafkaTest.Initializer.class})
@Import(TestTopicKafkaListener.class)
@SpringBootTest
public class KafkaTest {

	@Test
	@Timeout(1000)
	void testSendAndReceiveMessageWithContainerizedKafka(
			@Autowired EventRegistry eventRegistry,
			@Autowired KafkaTemplate<?, String> kafkaTemplate)
			throws InterruptedException {
		String nowString = Instant.now().toString();
		//
		kafkaTemplate.send(TestTopicKafkaListener.TOPIC_NAME, nowString);
		//
		// FIXME: Find smarter solution?
		Thread.sleep(2000);
		//
		Assertions.assertEquals(
				List.of(
						"test-topic-listener-initialized",
						"test-topic-receive-message: " + nowString),
				eventRegistry.getEvents());
	}

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse(
					"confluentinc/cp-kafka:latest"));
			kafkaContainer.start();
			TestPropertyValues
					.of(
							"spring.kafka.bootstrap-servers=" + kafkaContainer.getBootstrapServers(),
							//
							// Without this property listener does not read messages eagerly. Not sure why.
							"spring.kafka.consumer.auto-offset-reset=earliest")
					.applyTo(configurableApplicationContext.getEnvironment());
		}
	}
}
