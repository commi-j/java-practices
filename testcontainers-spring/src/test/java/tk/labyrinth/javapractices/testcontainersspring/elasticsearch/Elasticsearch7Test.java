package tk.labyrinth.javapractices.testcontainersspring.elasticsearch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.utility.DockerImageName;

/**
 * Removal of <b>_type</b> field in 8 ES server breaks default Spring client behaviour
 * (gives NPE when trying to read _type of created object):<br>
 * https://www.elastic.co/guide/en/elasticsearch/reference/7.17/removal-of-types.html<br>
 * It works fine with ES 7.<br>
 * <br>
 * https://www.baeldung.com/spring-data-elasticsearch-tutorial<br>
 * https://www.luminis.eu/blog/search-en/elasticsearch-instances-for-integration-testing/ - mentioned <b>spring.elasticsearch.rest.uris</b>.
 * https://stackoverflow.com/questions/43323099/elasticsearch-and-testcontainers-for-testing-none-of-the-configured-nodes-are-a - mentioned <b>xpack.security.enabled</b>.
 */
@ContextConfiguration(initializers = {Elasticsearch7Test.Initializer.class})
@SpringBootTest
public class Elasticsearch7Test {

	@Test
	@Timeout(1000)
	void testSaveObjectWithContainerizedElasticsearch(
			@Autowired ElasticsearchOperations elasticsearchOperations,
			@Autowired ElasticsearchObjectRepository elasticsearchObjectRepository)
			throws InterruptedException {
		Assertions.assertEquals(0, elasticsearchObjectRepository.count());
		//
		ElasticsearchObject saveResultWithUid = elasticsearchOperations.save(ElasticsearchObject.builder()
				.code("test-code")
				.name("test-name")
				.uid("test-uid")
				.build());
		ElasticsearchObject saveResultWithoutUid = elasticsearchOperations.save(ElasticsearchObject.builder()
				.code("test-code")
				.name("test-name")
				.uid(null)
				.build());
		//
		// Without sleep it may return 0.
		// FIXME: Find smarter solution?
		Thread.sleep(1000);
		//
		Assertions.assertEquals(2, elasticsearchObjectRepository.count());
	}

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			ElasticsearchContainer elasticsearchContainer = new ElasticsearchContainer(
					DockerImageName.parse("docker.elastic.co/elasticsearch/elasticsearch:7.17.5"))
					//
					// We need this one to make startup check and client connection work.
					.withEnv("xpack.security.enabled", "false");
			elasticsearchContainer.start();
			TestPropertyValues
					.of(
							"spring.elasticsearch.rest.uris=" + elasticsearchContainer.getHttpHostAddress())
					.applyTo(configurableApplicationContext.getEnvironment());
		}
	}
}
