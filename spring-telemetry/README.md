# spring-telemetry

## Tags

Perform case-sensitive search for specific tag to see relevant resources.

### LOGBACK-JSON

Writing logs in json format so that they can be easily processed by log collectors.

- Maven dependency 'ch.qos.logback.contrib:logback-jackson';
- Maven dependency 'ch.qos.logback.contrib:logback-json-classic';
- Main resources file 'logback.xml' (appender);

Guide: https://www.baeldung.com/java-log-json-output.

### LOGBACK-JAEGER-JSON

Outputting Jaeger's traceId/spanId in logs.

! Depends on LOGBACK-JSON.

- Maven dependency 'io.opentracing.contrib:opentracing-spring-jaeger-cloud-starter';
- Maven dependency 'net.logstash.logback:logstash-logback-encoder';
- Maven dependency 'org.codehaus.janino:janino';
- Java class 'MdcScopeManagerSettingTracerBuilderCustomizer';
- Main resources file 'logback.xml' (appender configuration);

Settings: JaegerAutoConfiguration

Guide: https://github.com/jaegertracing/jaeger-client-java/tree/master/jaeger-core#log-correlation

### MICROMETER

- Maven dependency 'io.micrometer:micrometer-core';

### MICROMETER-ASPECTS

! Depends on MICROMETER.

- Maven dependency 'org.springframework.boot:spring-boot-starter-aop';
- Java class 'MicrometerAspectsConfiguration';

Guide: https://micrometer.io/docs/concepts#_the_timed_annotation