package tk.labyrinth.javapractices.springrest.inbound;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * No solution from here works:<br>
 * https://www.baeldung.com/exception-handling-for-rest-with-spring<br>
 * Because we don't actually have an exception here, we just don't find corresponding resource in this method:<br>
 * org.springframework.web.servlet.resource.ResourceHttpRequestHandler#handleRequest(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)<br>
 */
@RestController
@RequestMapping("optionalPathVariable")
public class RestControllerWithOptionalPathVariable {

	@GetMapping("get/{pathVariable}")
	public String get(
			@PathVariable String pathVariable) {
		// Handles the following cases:
		//  /get/{pathVariable}
		//  /get/{pathVariable}/
		// Does not handle the following case:
		//  /get/{pathVariable}/smthMore
		//
		return "pathVariable = " + (pathVariable != null ? "\"" + pathVariable + "\"" : null);
	}

	@GetMapping("get")
	public String getWithNoPathVariable() {
		// Handles the following cases:
		//  /get
		//  /get/
		//
		return "pathVariable = null";
	}
}
