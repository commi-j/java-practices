package tk.labyrinth.javapractices.misc4j.java.lang;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ThreadUtils {

	/**
	 * Variant of {@link Thread#sleep(long)} with no checked exception.
	 *
	 * @param millis the length of time to sleep in milliseconds
	 *
	 * @throws RuntimeException instead of {@link InterruptedException}
	 */
	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}
}
