package tk.labyrinth.javapractices.testcontainersspring.elasticsearch;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ElasticsearchObjectRepository extends ElasticsearchRepository<ElasticsearchObject, String> {
	// empty
	// TODO: Change ID to UUID.
}
