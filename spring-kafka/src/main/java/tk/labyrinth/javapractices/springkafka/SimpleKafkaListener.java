package tk.labyrinth.javapractices.springkafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.Message;

import java.util.concurrent.atomic.AtomicInteger;

@RequiredArgsConstructor
@Slf4j
public class SimpleKafkaListener {

	private final AtomicInteger receivedMessageCounter = new AtomicInteger(0);

	private final TestCompletionHandle testCompletionHandle;

	@Value("${testRun.messageNumber}")
	private Integer expectedMessageNumber;

	@KafkaListener(topics = "topic.test", groupId = "simple")
	public void listen(Message<String> message) throws InterruptedException {
		int receivedMessageCount = receivedMessageCounter.incrementAndGet();
		logger.info("Receiving #{}", receivedMessageCount - 1);
		Thread.sleep(100);
		if (receivedMessageCount >= expectedMessageNumber) {
			logger.info("Receiving finished");
			testCompletionHandle.getFuture().complete(null);
		}
	}
}
