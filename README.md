# java-practices

## Modules

- micrometer - studies and practices of using https://micrometer.io/;
- parent-spring - parent for modules with Spring Boot;
- spring-telemetry - module with logging, tracing and metrics features built upon Spring Boot stack;