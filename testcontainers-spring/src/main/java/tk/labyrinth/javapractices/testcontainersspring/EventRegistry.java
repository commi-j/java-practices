package tk.labyrinth.javapractices.testcontainersspring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Slf4j
public class EventRegistry {

	private final AtomicInteger eventCount = new AtomicInteger(0);

	private final CopyOnWriteArrayList<String> events = new CopyOnWriteArrayList<>();

	public List<String> getEvents() {
		return new ArrayList<>(events);
	}

	public void writeEvent(String event) {
		logger.info(event);
		events.add(event);
	}
}
