package tk.labyrinth.javapractices.springmultipledatabases;

import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import tk.labyrinth.javapractices.springmultipledatabases.configuration.DatabaseConfiguration;

/**
 * This test shows how you can declare multiple {@link HikariDataSource HikariDataSources} that use both
 * specific (having unique infix) and generic Hikari properties.<br>
 * See {@link DatabaseConfiguration} and <b>application.properties</b> for details.
 */
class SpringMultipleDatabasesApplicationTest {

	@Test
	void testApplication() {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringMultipleDatabasesApplication.class);
		{
			// Blue singleton.
			//
			HikariDataSource blueDataSource = applicationContext.getBean("blueDataSource", HikariDataSource.class);
			//
			Assertions.assertNotNull(blueDataSource);
			Assertions.assertEquals("BLUE_POOL", blueDataSource.getPoolName());
			Assertions.assertEquals(30001, blueDataSource.getConnectionTimeout());
		}
		{
			// Red singleton.
			//
			HikariDataSource redDataSource = applicationContext.getBean("redDataSource", HikariDataSource.class);
			//
			Assertions.assertNotNull(redDataSource);
			Assertions.assertEquals("RED_POOL", redDataSource.getPoolName());
			Assertions.assertEquals(30002, redDataSource.getConnectionTimeout());
		}
		{
			// Base prototypes.
			//
			HikariDataSource baseDataSource0 = applicationContext.getBean("baseDataSource", HikariDataSource.class);
			HikariDataSource baseDataSource1 = applicationContext.getBean("baseDataSource", HikariDataSource.class);
			//
			Assertions.assertNotSame(baseDataSource0, baseDataSource1);
		}
	}
}
