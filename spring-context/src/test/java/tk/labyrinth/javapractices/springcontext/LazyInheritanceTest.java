package tk.labyrinth.javapractices.springcontext;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Summary: @Lazy inheritance does not work.
 */
class LazyInheritanceTest {

	@Test
	void myTest() {
		ConfigurableApplicationContext run = SpringApplication.run(
				new Class[]{
						ClassExtendingLazyAbstractClass.class,
						LazyClass.class
				},
				new String[]{});
		{
			BeanDefinition beanDefinition = run.getBeanFactory().getBeanDefinition("classExtendingLazyAbstractClass");
			Assertions.assertFalse(beanDefinition.isLazyInit());
		}
		{
			BeanDefinition beanDefinition = run.getBeanFactory().getBeanDefinition("lazyClass");
			Assertions.assertTrue(beanDefinition.isLazyInit());
		}
	}
}