package tk.labyrinth.javapractices.springtelemetry.micrometer;

import io.micrometer.core.aop.CountedAspect;
import io.micrometer.core.aop.TimedAspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * MICROMETER-ASPECTS<br>
 * <a href="https://micrometer.io/docs/concepts#_the_timed_annotation">https://micrometer.io/docs/concepts#_the_timed_annotation</a><br>
 *
 * @author Commitman
 * @version 1.0.0
 */
@Configuration
@Import({
		CountedAspect.class,
		TimedAspect.class,
})
public class MicrometerAspectsConfiguration {
	// empty
}
