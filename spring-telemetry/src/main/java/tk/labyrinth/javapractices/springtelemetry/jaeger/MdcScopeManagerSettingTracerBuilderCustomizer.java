package tk.labyrinth.javapractices.springtelemetry.jaeger;

import io.jaegertracing.internal.JaegerTracer;
import io.jaegertracing.internal.MDCScopeManager;
import io.opentracing.ScopeManager;
import io.opentracing.contrib.java.spring.jaeger.starter.TracerBuilderCustomizer;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * LOGBACK-JAEGER-JSON<br>
 * <a href="https://github.com/jaegertracing/jaeger-client-java/tree/master/jaeger-core#log-correlation">https://github.com/jaegertracing/jaeger-client-java/tree/master/jaeger-core#log-correlation</a><br>
 *
 * @author Commitman
 * @version 1.0.0
 */
@Component
@Lazy
public class MdcScopeManagerSettingTracerBuilderCustomizer implements TracerBuilderCustomizer {

	@Override
	public void customize(JaegerTracer.Builder builder) {
		Field scopeManagerField = Objects.requireNonNull(ReflectionUtils.findField(
				JaegerTracer.Builder.class,
				"scopeManager"));
		ReflectionUtils.makeAccessible(scopeManagerField);
		ScopeManager currentScopeManager = (ScopeManager) ReflectionUtils.getField(scopeManagerField, builder);
		//
		MDCScopeManager.Builder mdcScopeManagerBuilder = new MDCScopeManager.Builder();
		if (currentScopeManager != null) {
			mdcScopeManagerBuilder.withScopeManager(currentScopeManager);
		}
		builder.withScopeManager(mdcScopeManagerBuilder.build());
	}
}
