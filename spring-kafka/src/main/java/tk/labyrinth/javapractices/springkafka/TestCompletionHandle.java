package tk.labyrinth.javapractices.springkafka;

import lombok.Getter;

import java.util.concurrent.CompletableFuture;

public class TestCompletionHandle {

	@Getter
	private final CompletableFuture<Void> future = new CompletableFuture<>();
}
