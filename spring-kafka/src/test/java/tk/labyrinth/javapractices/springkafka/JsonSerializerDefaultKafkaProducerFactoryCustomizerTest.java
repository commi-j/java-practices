package tk.labyrinth.javapractices.springkafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Builder;
import lombok.Setter;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

@Slf4j
@Testcontainers
class JsonSerializerDefaultKafkaProducerFactoryCustomizerTest {

	@Container
	private final KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse(
			"confluentinc/cp-kafka:latest"));

	/**
	 * Verifying {@link Instant} is passed through Kafka in ISO-8601 format.
	 */
	@Test
	void testSerializeValueWithInstant() throws ExecutionException, InterruptedException {
		Instant startedAt = Instant.now();
		//
		SpringApplication springApplication = new SpringApplication(
				KafkaAutoConfiguration.class,
				JsonSerializerDefaultKafkaProducerFactoryCustomizer.class,
				ObjectMapperConfiguration.class,
				SimpleKafkaReceiver.class,
				TestKafkaSender.class);
		springApplication.setDefaultProperties(Map.of(
				"spring.kafka.bootstrapServers", kafkaContainer.getBootstrapServers(),
				"spring.kafka.consumer.autoOffsetReset", "earliest",
				"topic.test", "topic.testSerializeValueWithInstant"));
		//
		ConfigurableApplicationContext applicationContext = springApplication.run();
		//
		CompletableFuture<String> future = new CompletableFuture<>();
		//
		applicationContext.getBean(SimpleKafkaReceiver.class).setOnMessage(nextMessage ->
				future.complete(nextMessage.getPayload()));
		//
		applicationContext.getBean(TestKafkaSender.class).send(TestObject.builder()
				.instant(startedAt)
				.build());
		//
		String receivedPayload = future.get();
		//
		Assertions.assertEquals(
				"""
						{"instant":"%s"}""".formatted(startedAt.toString()),
				receivedPayload);
		//
		logger.info("Test finished in {}ms", Duration.between(startedAt, Instant.now()).toMillis());
	}

	@Configuration
	public static class ObjectMapperConfiguration {

		@Bean
		public static ObjectMapper objectMapper() {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.registerModule(new JavaTimeModule());
			objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
			return objectMapper;
		}
	}

	public static class SimpleKafkaReceiver {

		@Setter
		private volatile Consumer<Message<String>> onMessage = null;

		@KafkaListener(topics = "${topic.test}", groupId = "simple")
		private void receive(Message<String> message) {
			Consumer<Message<String>> onMessage = this.onMessage;
			if (onMessage != null) {
				onMessage.accept(message);
			}
		}
	}

	public static abstract class KafkaSenderBase<V> {

		@Autowired
		private KafkaTemplate<Void, V> kafkaTemplate;

		@org.springframework.beans.factory.annotation.Value("${topic.test}")
		private String topicName;

		public void send(V payload) {
			kafkaTemplate.send(MessageBuilder
					.withPayload(payload)
					.setHeader(KafkaHeaders.TOPIC, topicName)
					.build());
		}
	}

	public static class TestKafkaSender extends KafkaSenderBase<TestObject> {
		// empty
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class TestObject {

		Instant instant;
	}
}