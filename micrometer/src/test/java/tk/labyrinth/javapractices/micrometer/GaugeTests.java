package tk.labyrinth.javapractices.micrometer;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicLong;

class GaugeTests {

	/**
	 * Verifying Gauge metric can not be reregistered.
	 */
	@Test
	void testGaugeRegisteredTwice() {
		SimpleMeterRegistry meterRegistry = new SimpleMeterRegistry();
		Metrics.addRegistry(meterRegistry);
		//
		AtomicLong atomicLong0 = new AtomicLong(15);
		AtomicLong atomicLong1 = new AtomicLong(20);
		//
		AtomicLong gaugeValue0 = Metrics.gauge("test", atomicLong0);
		AtomicLong gaugeValue1 = Metrics.gauge("test", atomicLong1);
		//
		Meter meter = meterRegistry.getMeters().get(0);
		double value = meter.measure().iterator().next().getValue();
		//
		Assertions.assertSame(atomicLong0, gaugeValue0);
		Assertions.assertSame(atomicLong1, gaugeValue1);
		Assertions.assertEquals(15d, value);
	}
}