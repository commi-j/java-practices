package tk.labyrinth.javapractices.testcontainersspring.kafka;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import tk.labyrinth.javapractices.testcontainersspring.EventRegistry;

import javax.annotation.PostConstruct;

@Component
@Lazy
@RequiredArgsConstructor
public class TestTopicKafkaListener {

	public static final String TOPIC_NAME = "test-topic";

	private final EventRegistry eventRegistry;

	@KafkaListener(id = "test-topic-listener", topics = TOPIC_NAME)
	private void listenTestTopic(String message) {
		eventRegistry.writeEvent("test-topic-receive-message: " + message);
	}

	@PostConstruct
	private void postConstruct() {
		eventRegistry.writeEvent("test-topic-listener-initialized");
	}
}
