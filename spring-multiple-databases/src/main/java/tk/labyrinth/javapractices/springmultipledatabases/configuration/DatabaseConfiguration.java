package tk.labyrinth.javapractices.springmultipledatabases.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.jdbc.DataSourceConfigurationAccessor;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

@Configuration
public class DatabaseConfiguration {

	/**
	 * See org.springframework.boot.autoconfigure.jdbc.DataSourceConfiguration.Hikari.
	 */
	@Bean("baseDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.hikari")
	@Scope(BeanDefinition.SCOPE_PROTOTYPE)
	HikariDataSource baseDataSource(DataSourceProperties properties) {
		HikariDataSource dataSource = DataSourceConfigurationAccessor.createDataSource(properties, HikariDataSource.class);
		if (StringUtils.hasText(properties.getName())) {
			dataSource.setPoolName(properties.getName());
		}
		return dataSource;
	}

	@Bean("blueDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.hikari.blue")
	HikariDataSource blueDataSource(HikariDataSource baseDataSource) {
		return baseDataSource;
	}

	@Bean("redDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.hikari.red")
	HikariDataSource redDataSource(HikariDataSource baseDataSource) {
		return baseDataSource;
	}
}
