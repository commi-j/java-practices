package tk.labyrinth.javapractices.testcontainersspring;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@EnableElasticsearchRepositories
@SpringBootApplication
public class TestcontainersSpringApplication {
	// empty
}
