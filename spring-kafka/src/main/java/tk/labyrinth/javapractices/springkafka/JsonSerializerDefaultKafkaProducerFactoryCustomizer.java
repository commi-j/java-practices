package tk.labyrinth.javapractices.springkafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.kafka.DefaultKafkaProducerFactoryCustomizer;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.stereotype.Component;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Component
@Lazy
@RequiredArgsConstructor
public class JsonSerializerDefaultKafkaProducerFactoryCustomizer implements DefaultKafkaProducerFactoryCustomizer {

	private final ObjectMapper objectMapper;

	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void customize(DefaultKafkaProducerFactory<?, ?> producerFactory) {
		JsonSerializer jsonSerializer = new JsonSerializer<>(objectMapper);
		producerFactory.setKeySerializer(jsonSerializer);
		producerFactory.setValueSerializer(jsonSerializer);
	}
}
