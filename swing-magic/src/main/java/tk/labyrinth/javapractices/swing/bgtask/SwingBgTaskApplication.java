package tk.labyrinth.javapractices.swing.bgtask;

import tk.labyrinth.javapractices.misc4j.java.lang.ThreadUtils;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.ForkJoinPool;

/**
 * https://docs.oracle.com/javase/tutorial/uiswing/concurrency/index.html
 */
public class SwingBgTaskApplication {

	private static final long sleepTime = 5000;

	public static void main(String... args) {
		JFrame frame = new JFrame();
		{
			frame.setLayout(new FlowLayout());
		}
		{
			JButton button = new JButton("Execute in UI Thread");
			button.addActionListener(event -> {
				button.setEnabled(false);
				//
				// Executing in FJP Thread.
				ThreadUtils.sleep(sleepTime);
				//
				button.setEnabled(true);
			});
			frame.add(button);
		}
		{
			JButton button = new JButton("Execute in Background Thread");
			button.addActionListener(event -> {
				button.setEnabled(false);
				//
				// We can use SwingWorker, but why?
				ForkJoinPool.commonPool().execute(() -> {
					//
					// Executing in FJP Thread.
					ThreadUtils.sleep(sleepTime);
					//
					SwingUtilities.invokeLater(() -> {
						//
						// Changing UI in UI Thread.
						button.setEnabled(true);
					});
				});
			});
			frame.add(button);
		}
		{
			frame.add(new JLabel("Try resizing this frame after clicking different buttons."));
		}
		{
			frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			frame.setSize(400, 300);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
		}
	}
}
